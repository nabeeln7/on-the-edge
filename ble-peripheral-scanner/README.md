# BLE Peripheral Scanner
This module performs the following operations:
1. Scans for BLE peripherals (estimotes, lighting sensors) 
2. Publishes the sensor data on the MQTT topic "gateway-data"