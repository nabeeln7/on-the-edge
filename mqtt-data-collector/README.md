# MQTT Data Collector
This module subscribes to the "gateway-data" mqtt topic and adds a stripped down version of the data points to the database.

The "gateway-data" topic has the sensor data from the ble-peripheral-scanner and also from the lab11 gateway project. 